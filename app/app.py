#!/usr/bin/env python3
""" ReaQta Web Application """
from flask import Flask, jsonify, make_response, render_template


APP = Flask(__name__)
APP.config.from_object('config.ProductionConfig')

@APP.errorhandler(404)
def not_found(error):
    """ Function to Not Found response """
    return make_response(jsonify({'error': 'Not Found'}), 404)


@APP.route('/', methods=['GET'])
def index():
    """ Function to respond the root resoure the APIs """
    return render_template('index.html')


@APP.route('/api', methods=['GET'])
def api():
    """ Function to respond the root resoure the APIs """
    return render_template('api.html')


if __name__ == '__main__':
    APP.run()
