#!/usr/bin/env python3
""" Unit tests for Custmers APIs """
import flask_unittest

from flask import current_app as APP


class TestReaQta(flask_unittest.ClientTestCase):
    """ Testing ReaQta web application """

    APP.config.from_object('config.TestingConfig')

    def test_home_page(self, client):
        """ unit test for GET '/' """

        response = client.get('/')
        assert response.status_code == 200
        assert b"Welcome to ReaQta" in response.data

    def test_api_page(self, client):
        """ unit test for GET '/api' """
        response = client.get('/')
        assert response.status_code == 200
        assert b"Welcome to ReaQta" in response.data
