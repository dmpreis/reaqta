#!/usr/bin/env python3
""" Module with the configuration's parameter of the API """


class Config(object):
    """ Default Configuration """
    DEBUG = False
    TESTING = False


class ProductionConfig(Config):
    """ Production Environment Configuration """


class DevelopmentConfig(Config):
    """ Development EnvironmentConfiguration """
    DEBUG = True


class TestingConfig(Config):
    """ Testing Environment Configuration """
    TESTING = True
