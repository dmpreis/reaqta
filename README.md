# reaqta



## Stack 

- **Infra provsioner:** Terraform
- **Container Orchestrator:** Kubernetes (AWS EKS)
- **COntainer image builder:** Docker
- **Web Application:** Python/Flask

## Pre requisites

- Terraform CLI installed
- AWS CLI v2 installed
- AWS IAM credentials configured 
- kubectl installed

## Run locally

To run the project in your local environment execute the following commands:

```bash
make build
make up
curl http://127.0.0.1/
```

## Using all stack

In order to user the K8s provisioned by Terraform in aws, execute the following commands:

```bash
make all
```

This target will provision a EKS cluster on AWS, configure kubectl, deploy metrics-server, the application and create a service using classic loadbalancer to access the application.
After executing all the steps the application's URL will be shown in th output.